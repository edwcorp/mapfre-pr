<?php

namespace App\Http\Middleware\Front;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckForCookieToken {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
		if(Auth::user() === null) {
			$token = $request->cookie('login_data');

			if($token === null) 
				return redirect(route('front.main.showSesionForm'));

			$user = User::where('remember_token', $token)->first();

			if($user === null)
				return redirect(route('front.main.showSesionForm'));

			Auth::setUser($user);
		}
		
        return $next($request);
    }
}
