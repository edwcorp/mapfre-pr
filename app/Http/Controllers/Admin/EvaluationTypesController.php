<?php

namespace App\Http\Controllers\Admin;

use App\Models\EvaluationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EvaluationTypesController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EvaluationType  $evaluationType
     * @return \Illuminate\Http\Response
     */
    public function show(EvaluationType $evaluationType) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EvaluationType  $evaluationType
     * @return \Illuminate\Http\Response
     */
    public function edit(EvaluationType $evaluationType) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EvaluationType  $evaluationType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EvaluationType $evaluationType) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EvaluationType  $evaluationType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EvaluationType $evaluationType) {
        //
    }
}
