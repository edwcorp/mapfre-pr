<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

	public function login(Request $request) {
		$inputs = $request->all();

		$rules = [
			'email' => 'bail|required|email',
			'password' => 'bail|required'
		];

		$messages = [
			'required'    => 'Este campo es obligatorio.',
			'email'    => 'Ingrese un email válido'
		];

		$validator = Validator::make($inputs, $rules, $messages);

		// if any data is wrong
		if ($validator->fails())
			return back()->withErrors($validator, 'data')->withInput($inputs);

		$email = $inputs['email'];
		$password = hash('sha256', $inputs['password']);

		$user = User::where([
			[
				'email',
				$email
			],
			[
				'password',
				$password
			],
			[
				'is_admin',
				true
			]
		])->first();
	}

	public function ranking() {

	}
}
