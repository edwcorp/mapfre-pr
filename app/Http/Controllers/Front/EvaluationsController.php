<?php

namespace App\Http\Controllers\Front;

use App\User;
use App\Models\Answer;
use App\Models\Evaluation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EvaluationsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $evaluationId) {
		$evaluation = Evaluation::findOrFail($evaluationId);
		
		// Check if the user can access this evaluation
		$user = Auth::user();

		if($evaluation->isEnabled() && $evaluation->canAccess($user))
			return view($this->_config['view'], compact('evaluation'));
		else
			Evaluation::findOrFail(-1);
	}
	
	/**
     * Store the answers for an especific resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function answer(Request $request, $evaluationId) {
		$evaluation = Evaluation::findOrFail($evaluationId);
		
		// Check if the user can access this evaluation
		$user = Auth::user();

		if(!$evaluation->canAccess($user))
			Evaluation::findOrFail(-1);

		$inputs = $request->all();
		$questions = $evaluation->activeQuestions();
		$answers = [];

		// process and validate answers for questions
		foreach($questions as $question) {
			if($question->isSimple()) {
				$checkingInput = 'question_'.$question->id;
				if(!isset($inputs[$checkingInput]))
					return back()
							->withError("Esta pregunta es obligatoria", $checkingInput)
							->withInput($inputs);

				$optionId = $inputs[$checkingInput];

				$option = $question->options()
									->where('id', $optionId)
									->where('active', true)
									->first();

				if($option === null)
					return back()
							->withError("Escoja una respuesta entre las opciones", $checkingInput)
							->withInput($inputs);

				if($option->is_open) {
					$checkingAnswer = $checkingInput .'_answer_'.$option->id;
					$openAnswer = $inputs[$checkingAnswer];
				} else
					$openAnswer = null;

				$answer = new Answer();
				$answer->forQuestion($question);
				$answer->setAnswer($option, $openAnswer);
				$answer->answeredBy($user);

				$answers[] = $answer;
			}
		}

		// if we get here, save all the answers
		foreach($answers as $answer) {
			$answer->save();
		}

		// Enable next evaluation. This is just for MAPFRE
		$this->enableNextEvaluationFor($user, $evaluation);

		return redirect(route($this->_config['redirect'], $evaluationId));
	}

	/**
     * Display the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function results(Request $request, $evaluationId) {
		$evaluation = Evaluation::findOrFail($evaluationId);
		
		$user = Auth::user();

		$answers = Answer::where([
			[
				'evaluation_id', 
				$evaluation->id
			],
			[
				'filled_by',
				$user->id
			]
		])->get();

		// user haven't answered this evaluation yet
		if(count($answers) === 0)
			return redirect()->route($this->_config['redirect'], $evaluationId);

		// Count the final value of all the evaluation
		$correctAnswers = $answers->reduce(
			function ($carry, $answer) {
				return $carry + $answer->value;
			}, 
			0
		);

		$vars = [
			'evaluation',
			'correctAnswers'
		];

		return view($this->_config['view'], compact($vars));
	}
	
	public function enableNextEvaluationFor(User $user, $lastEvaluation) {
		$nextEvaluation = Evaluation::where('id', '>', $lastEvaluation->id)->first();

		$query = $lastEvaluation->users()->where('user_id', $user->id);

		if($query->count() === 0)
			$lastEvaluation->users()->attach($user->id, ['active' => false]);
		else
			$lastEvaluation->users()->where('user_id', $user->id)->update(['active' => false]);

		if($nextEvaluation !== null)
			$nextEvaluation->users()->attach($user->id);
	}
}
