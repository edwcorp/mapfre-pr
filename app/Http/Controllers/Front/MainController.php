<?php

namespace App\Http\Controllers\Front;

#use App\Models\Category;
use App\User;
use App\Models\Answer;
use App\Models\Evaluation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegisterForm() {
        return view($this->_config['view']);
	}
	
	/**
     * Register a new participant to the system
     *
     * @return \Illuminate\Http\Response
     */
	public function storeRegisterForm(Request $request) {
		$inputs = $request->all();

		$rules = [
			'nombres' => 'bail|required',
			'email' => 'bail|required|email',
			'telefono' => 'bail|required|integer',
			'documento' => 'bail|required|integer',
			'terminos' => 'bail|required'
		];

		$messages = [
			'required'    => 'Este campo es obligatorio.',
			'email'    => 'Ingrese un email válido',
			'integer' => 'Ingrese un número.'
		];

		$validator = Validator::make($inputs, $rules, $messages);

		// if any data is wrong
		if ($validator->fails()) {
			return back()->withErrors($validator, 'data')->withInput($inputs);
		}

		$documentId = $inputs['documento'];
		$name = $inputs['nombres'];
		$email = $inputs['email'];
		$phone = $inputs['telefono'];

		$user = User::where('document_id', $documentId)->first();

		if($user !== null) 
			return back()->withErrors('Documento ya registrado', 'document')->withInput($inputs);

		$user = User::where('email', $email)->first();

		if($user !== null)
			return back()->withErrors('Correo ya registrado', 'email')->withInput($inputs);

		$user = new User();

		$user->name = $name;
		$user->phone = $phone;
		$user->email = $email;
		$user->document_id = $documentId;
		
		$token = Str::random(100);

		$user->password = hash('sha256', $token);
		$user->remember_token = $token;

		$user->save();

		$firstEvaluation = Evaluation::find(1);
		$firstEvaluation->users()->attach($user->id);

		return redirect(route($this->_config['redirect']))
				->withCookie('login_data', $token);
	}
	
	/**
     * Login a participant with the email
     *
     * @return \Illuminate\Http\Response
     */
	public function login(Request $request) {
		$inputs = $request->all();

		$rules = [
			'email' => 'bail|required|email'
		];

		$messages = [
			'required'    => 'Este campo es obligatorio.',
			'email'    => 'Ingrese un email válido'
		];

		$validator = Validator::make($inputs, $rules, $messages);

		// if any data is wrong
		if ($validator->fails())
			return back()->withErrors($validator, 'data')->withInput($inputs);

		$email = $inputs['email'];

		$user = User::where('email', $email)->first();

		if($user === null)
			return back()->withErrors('El correo no se encuentra registrado', 'email')->withInput($inputs);

		// User was found and set him to the session
		Auth::setUser($user);

		// Generate token for the cookie
		$token = Str::random(100);

		// Save token in DB for further access
		$user->password = hash('sha256', $token);
		$user->remember_token = $token;
		$user->save();

		// Expires in 1 week (60 mins * 24 hours * 7 days)
		$expiresIn = 60 * 24 * 7;
		$cookie = cookie('login_data', $token, $expiresIn);

		return redirect(route($this->_config['redirect']))
				// Set token to the response cookie
				->withCookie($cookie);	
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showListTrivias() {
		$user = Auth::user();

		$completedEvaluations = $user->evaluations()->where('evaluation_user.active', false)->get();
		$currentEvaluation = $user->evaluations()->where('evaluation_user.active', true)->first();

		$vars = [
			'completedEvaluations',
			'currentEvaluation'
		];

        return view($this->_config['view'], compact($vars));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSesionForm() {
        return view($this->_config['view']);  
	}
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showThankYou() {
        return view($this->_config['view']);
	}

	public function winners() {
		// Get all users 

		/* 
			select 
				filled_by, 
				count(distinct(evaluation_id)) as trivias, 
				sum(value) as points 
			from 
				answers 
			group by 
				filled_by 
			order by 
				trivias desc, 
				points desc;
		 */
		$results = DB::table('answers')->select(
			DB::raw('filled_by, count(distinct(evaluation_id)) as trivias, sum(value) as points')
		)->groupBy('filled_by')
		 ->orderBy('trivias', 'DESC')
		 ->orderBy('points', 'DESC')
		 ->take(20)
		 ->get();

		if($results->count() > 0) {
			$reference = $results->first();
			$firstPlaces = $results->filter(function ($item) use($reference) {
				return $item->trivias === $reference->trivias && $item->points === $reference->points;
			});

			// there's a tie in the first position
			if($firstPlaces->count() > 1) {
				// Remove the first elements
				for($i = 0; $i < $firstPlaces->count(); $i++){
					$results->pull($i);
				}

				$firstPlaces = $this->solveTie($firstPlaces);
				$results = $firstPlaces->concat($results);
			}
		}

		$winners = $results->take(10);

		return view($this->_config['view'], compact('winners'));
	}

	/* 
		Calculate a calification based on a referenced time
		Then sort the Collection $firstPlaces by that calification ASC
	*/
	public function solveTie(Collection $firstPlaces) {
		$newOrder = new Collection([]);
		// Get only Users Id
		$usersIds = $firstPlaces->pluck('filled_by');
		$calification = [];

		for($evaluation_id = 1; $evaluation_id <= 4; $evaluation_id++) {
			$comparation = null;
			foreach($usersIds as $user) {
				// get the answer of the user for that evaluation
				$answer = Answer::where([
					[
						'evaluation_id',
						$evaluation_id
					],
					[
						'filled_by',
						$user
					]
				])->first();

				if($answer === null)
					continue;
				
				// If we're going throught the first answer, 
				// no comparation point is available
				if($comparation === null) {
					$comparation = new DateTime($answer->created_at);
					// Reference will always be zero
					$calification[$user] = 0;
				} else {
					if(!isset($calification[$user]))
						$calification[$user] = 0;
					$aux = new DateTime($answer->created_at);
					// Answers before the reference will be negativer. Newer answers will be greater
					$calification[$user] += ($aux->getTimestamp() - $comparation->getTimestamp());
				}
			}
		}

		// Sort by calification
		$newOrder = $firstPlaces->sort(function ($a, $b) use ($calification) {
			return $calification[$a->filled_by] - $calification[$b->filled_by];
		});

		return $newOrder->values();
	}
}
