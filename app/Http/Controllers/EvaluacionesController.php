<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Form;
use View;
use Validator;
use Input;
use Redirect;
use Session;
use DateTime;
use DB;
use Illuminate\Routing\Controller;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Evaluacion;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



class EvaluacionesController extends Controller {
	/* funciones del controlador */

	// Función por defecto del controlador , actua como el index del controlador
	public function get_index($idevaluacion, $idevaluador)
	{
		//$convocatoria = Convocatoria::find($idconvocatoria);
		//$concursos = Concurso::where('idconvocatoria','=',$idconvocatoria)->get(); //get entrega todos los resultados,  reemplazarlo por paginate
			//var_dump($usuarios);
		return View::make("evaluaciones.index");
	}

	// Función para el despliegue del formulario de evaluacion
	public function get_evaluacion($idevaluacion, $idevaluador, $idcolaborador)
	{
		//$convocatoria = Convocatoria::find($idconvocatoria);
		//$concursos = Concurso::where('idconvocatoria','=',$idconvocatoria)->get(); //get entrega todos los resultados,  reemplazarlo por paginate
			//var_dump($usuarios);
		return View::make("evaluaciones.evaluar");
	}

	/*public function get_crear($idconvocatoria)
	{
		$convocatoria = Convocatoria::find($idconvocatoria);
		return View::make("concursos.crear")->with('convocatoria', $convocatoria);
	}

	public function post_crear(Request $request, $idconvocatoria)
	{
		$recibir= Input::all();
		$mensaje = '';

		//creacion de reglas para validación en un array
		$reglas = array(
						'nombre' => 'required|min:3',
						'fechainicio' => 'required|before:fechacierre',
                        'fechacierre' => 'required|after:fechainicio',
                        'etapa_registro_inicio' => 'required|before:etapa_registro_fin',
                        'etapa_registro_fin' => 'required|after:etapa_registro_inicio',
                        'etapa_programacion_inicio' => 'required|before:etapa_programacion_fin',
                        'etapa_programacion_fin' => 'required|after:etapa_programacion_inicio',
                        'etapa_evidencias_inicio' => 'required|before:etapa_evidencias_fin',
                        'etapa_evidencias_fin' => 'required|after:etapa_evidencias_inicio',
                        'etapa_calificacion_inicio' => 'required|before:etapa_calificacion_fin',
						'etapa_calificacion_fin' => 'required|after:etapa_calificacion_inicio',
						'estado' => 'required',
					);

		//mensajes opcionales de validacion (sirve para cambiar los mensajes por defecto de una validación)
		$mensajes = array(
							'required'=>'Campo obligatorio',
							'min'=>'Este campo debe contener mínimo 3 caracteres',
							'before'=>'Fecha de inicio no puede superior a la fecha de cierre',
							'after'=>'Fecha de cierre no puede ser inferior a fecha de inicio',
						);
		//logica de validación
		$validar = Validator::make($recibir,$reglas,$mensajes); //los mensajes son opcionales

		if($validar->fails())
		{
			return Redirect::back()->withErrors($validar)->withInput(); //Si hay un error se redirecciona nuevamente al formulario mostrando los mensajes de error que ocurrieron
		}else{
			$success = false;
			DB::beginTransaction();
			try {
				//creamos el modelo
				$concurso = new Concurso;
				$concurso->nombre = Input::get('nombre');
				$concurso->estado = Input::get('estado');
				$concurso->fechainicio = date("Y-m-d H:i:s",strtotime(Input::get('fechainicio')));
                $concurso->fechacierre = date("Y-m-d H:i:s",strtotime(Input::get('fechacierre')));
                $concurso->etapa_registro_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_registro_inicio')));
                $concurso->etapa_registro_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_registro_fin')));
                $concurso->etapa_programacion_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_programacion_inicio')));
                $concurso->etapa_programacion_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_programacion_fin')));
                $concurso->etapa_evidencias_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_evidencias_inicio')));
                $concurso->etapa_evidencias_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_evidencias_fin')));
                $concurso->etapa_calificacion_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_calificacion_inicio')));
                $concurso->etapa_calificacion_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_calificacion_fin')));
				$concurso->idconvocatoria = $idconvocatoria;

				//guardamos en la base de datos con el método save
				if($concurso->save())
				{
					$convocatoria = Convocatoria::find($idconvocatoria)->first();

					// Crea Lista
					$listaRegistro = $this->createListMailchimp('registro', $concurso->idconcurso, $concurso->nombre, $convocatoria->nombre,config('constants.TEMPLATEMAILCHIMPETAPASREGISTRO'));
					$listaProgramacion = $this->createListMailchimp('programacion', $concurso->idconcurso, $concurso->nombre, $convocatoria->nombre,config('constants.TEMPLATEMAILCHIMPETAPASPROGRAMACION'));
					$listaEvidencias = $this->createListMailchimp('evidencias', $concurso->idconcurso, $concurso->nombre, $convocatoria->nombre,config('constants.TEMPLATEMAILCHIMPETAPASEVIDENCIAS'));
					$listaFinalizacion = $this->createListMailchimp('finalizacion', $concurso->idconcurso, $concurso->nombre, $convocatoria->nombre,config('constants.TEMPLATEMAILCHIMPETAPASFINALIZACION'));

					if ($listaRegistro['resultTransaction'] && $listaProgramacion['resultTransaction'] && $listaEvidencias['resultTransaction'] && $listaFinalizacion['resultTransaction']) {
						$success = true;
					} else {
						$mensaje = "Se ha presentado un error creando las listas y campañas de las etapas en Mailchimp.";
					}	

				} else {
					$mensaje = "Error registrando el concurso en la base de datos.";
				}
			} catch (\Exception $e) {
				// maybe log this exception, but basically it's just here so we can rollback if we get a surprise
				echo $e->getTraceAsString();
				exit();
			}

			//ahora creamos una variable de sesion antes de hacer la redirección
			if ($success) {
				DB::commit();
				Session::flash("mensaje","Concurso creado satisfactoriamente!");
				return Redirect::to('concursos/'. $concurso->idconvocatoria .'/listado');
			} else {
				DB::rollback();
				Session::flash("errordataform","El concurso no pudo ser creado, inténtalo nuevamente. " . $mensaje);
				return Redirect::to('concursos/'. $concurso->idconvocatoria .'/listado')->with('errordataform',"El concurso no pudo ser creado, inténtalo nuevamente. " . $mensaje); 
			}
		}
	}

	public function editar($id)
	{
		$concurso = Concurso::find($id);
		return View::make("concursos.editar")->with("concurso",$concurso);
	}

	public function actualizar($id,Request $request)
	{
		$recibir= Input::all();
		//creacion de reglas para validación en un array
		$reglas = array(
						'nombre' => 'required|min:3',
						'fechainicio' => 'required|before:fechacierre',
                        'fechacierre' => 'required|after:fechainicio',
                        'etapa_registro_inicio' => 'required|before:etapa_registro_fin',
                        'etapa_registro_fin' => 'required|after:etapa_registro_inicio',
                        'etapa_programacion_inicio' => 'required|before:etapa_programacion_fin',
                        'etapa_programacion_fin' => 'required|after:etapa_programacion_inicio',
                        'etapa_evidencias_inicio' => 'required|before:etapa_evidencias_fin',
                        'etapa_evidencias_fin' => 'required|after:etapa_evidencias_inicio',
                        'etapa_calificacion_inicio' => 'required|before:etapa_calificacion_fin',
						'etapa_calificacion_fin' => 'required|after:etapa_calificacion_inicio',
						'estado' => 'required',
					);

		//mensajes opcionales de validacion (sirve para cambiar los mensajes por defecto de una validación)
		$mensajes = array(
							'required'=>'Campo obligatorio',
							'min'=>'Este campo debe contener mínimo 3 caracteres',
							'before'=>'Fecha de inicio no puede superior a la fecha de cierre',
							'after'=>'Fecha de cierre no puede ser inferior a fecha de inicio',
						);
		//logica de validación
		$validar = Validator::make($recibir,$reglas,$mensajes); //los mensajes son opcionales

		if($validar->fails())
		{
			return Redirect::to('concursos/'.$id.'/editar')->withErrors($validar); //Si hay un error se redirecciona nuevamente al formulario mostrando los mensajes de error que ocurrieron
		}
		else{

			$success = false;
			DB::beginTransaction();
			try {
				//creamos el modelo
				$concurso = Concurso::find($id);
				$concurso->nombre = Input::get('nombre');
				$concurso->estado = Input::get('estado');
				$concurso->fechainicio = date("Y-m-d H:i:s",strtotime(Input::get('fechainicio')));
                $concurso->fechacierre = date("Y-m-d H:i:s",strtotime(Input::get('fechacierre')));
                $concurso->etapa_registro_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_registro_inicio')));
                $concurso->etapa_registro_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_registro_fin')));
                $concurso->etapa_programacion_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_programacion_inicio')));
                $concurso->etapa_programacion_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_programacion_fin')));
                $concurso->etapa_evidencias_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_evidencias_inicio')));
                $concurso->etapa_evidencias_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_evidencias_fin')));
                $concurso->etapa_calificacion_inicio = date("Y-m-d H:i:s",strtotime(Input::get('etapa_calificacion_inicio')));
                $concurso->etapa_calificacion_fin = date("Y-m-d H:i:s",strtotime(Input::get('etapa_calificacion_fin')));
				
				//guardamos en la base de datos con el método save
				if($concurso->save())
				{
					$success = true;
				}
			} catch (\Exception $e) {
				// maybe log this exception, but basically it's just here so we can rollback if we get a surprise
				echo $e->getTraceAsString();
				exit();
			}
			//ahora creamos una variable de sesion antes de hacer la redirección
			if ($success) {
				DB::commit();
				Session::flash("mensaje","Concurso actualizado satisfactoriamente!");
				return Redirect::to('concursos/'. $concurso->idconvocatoria .'/listado');
			} else {
				DB::rollback();
				Session::flash("errordataform","El concurso no pudo ser actualizado, inténtalo nuevamente.");
				$mensajeerror="La convocatoria no pudo ser actualizada, inténtalo nuevamente.";
				return Redirect::to('concursos/'. $concurso->idconvocatoria .'/listado')->with('errordataform',$mensajeerror); //Si hay un error se redirecciona nuevamente al formulario mostrando los mensajes de error que ocurrieron
			}
		}
	}

	public function mostrar($id){
		$concurso = Concurso::find($id);
		return View::make("concursos.mostrar")->with("concurso",$concurso);
	}

	public function eliminar($id, $idconvocatoria){
		$concurso = Concurso::find($id);
		$concurso->delete();
		Session::flash("mensaje","Concurso eliminado exitosamente!");
		//redireccionamos
		return Redirect::to('concursos/'.$idconvocatoria.'/listado');
	}

	public function createListMailchimp($etapa, $idConcurso, $nombreConcurso, $nombreConvocatoria, $idTemplateMailchimp) {
		$resultTransaction = false;
		$resultLista = '';
		// Se crea la lista en la DB
		$lista = new Notificacion;
		$lista->nombre_lista = "Convocatoria: " . $nombreConvocatoria . " - Etapa " . strtoupper($etapa) . " - Concurso (" . $idConcurso . ") - " . $nombreConcurso;
		$lista->etapa = $etapa;
		$lista->id_lista_mailchimp = null;
		$lista->id_campana_mailchimp = null;
		$lista->estado_lista = 'inactiva';
		$lista->estado_campana = 'inactiva';
		$lista->idconcurso = $idConcurso;
		$lista->idtallerprog = 0; 

		if($lista->save())
		{
			// Se debe crear la lista para el taller programación en mailchimp
			// MailChimp API credentials
			$apiKey = config('constants.APIKEYMAILCHIMP');
			
			// MailChimp API URL
			$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
			$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists';

			// member information
			$json = json_encode(['name' => "Lista Etapa " . strtoupper($etapa) . " - Concurso (" . $idConcurso . ") - " . $nombreConcurso,
								 'contact' => [	'company' => 'Corporación Juego y Niñez', 
												 'address1' => 'Calle 97 # 60d - 86', 
												 'city' => 'Bogotá', 
												 'state' => 'Bogotá', 
												 'zip' => '111211', 
												 'country' => 'Colombia'], 
												 'permission_reminder' => 'Recibes este correo electrónico porque te registraste para participar en el CONCURSO ALCALDE Y GOBERNADOR MÁS PILO 2019.', 
												 'campaign_defaults' => [	'from_name' => 'Corporación Juego y Niñez', 
																			 'from_email' => 'concurso@juegoyninez.org', 
																			 'subject' => 'Concurso Alcalde y Gobernador Pilo', 
																			 'language' => 'es'], 
																			 'email_type_option' => true]);

			// send a HTTP POST request with curl
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			$result = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			$responseMailchimp = json_decode($result, true);

			// store the status message based on response code
			if ($httpCode == 200) {
				// Se actualiza la tabla local con id de lista mailchimp
				$listaUpd = Notificacion::find($lista->idlistanotificacion);
				$listaUpd->id_lista_mailchimp = $responseMailchimp['id'];
				$listaUpd->estado_lista = 'activa';

				if($listaUpd->save())
				{
					$resultLista = $this->createCampaignMailchimp($etapa, $idConcurso, $nombreConcurso, $responseMailchimp['id'], $lista->idlistanotificacion, $idTemplateMailchimp);
					
					if ($resultLista['resultTransaction']) {
						$resultTransaction = true;
						$mensaje = "Creación de lista y campaña exitosa";
					} else {
						$mensaje = "La campaña no se ha podido crear para la etapa de " . $etapa;
					}
					
				} else {
					$mensaje = "La lista no se ha podido actualizar en la base de datos local para la etapa de " . $etapa;
				}
			}else {
				$mensaje = "La lista no se ha podido crear en mailchimp para la etapa de " . $etapa . ".";
			}
		} else {
			$mensaje = "La lista no se ha podido crear en la base de datos local para la etapa de " . $etapa . ".";
		}

		return ["resultTransaction" => $resultTransaction, "mensaje" => $mensaje];
	}

	public function createCampaignMailchimp($etapa, $idConcurso, $nombreConcurso, $idlista, $idListaNotificacion, $templateId) {
		$resultTransaction = false;
		// MailChimp API credentials
		$apiKey = config('constants.APIKEYMAILCHIMP');
			
		// MailChimp API URL
		$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);

		// Se debe crear la campaña para el taller programación en mailchimp
		$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/campaigns';
		// Campaign information
		$json = json_encode([	'type' => 'regular', 
								'recipients' => ['list_id' => $idlista], 
								'settings' => [	'subject_line' => "Recordatorio Concurso Alcalde y Gobernador Pilo", 
												'title' => "Campaña Etapa " . strtoupper($etapa) . " - Concurso (" . $idConcurso . ") - " . $nombreConcurso,
												'reply_to' => 'concurso@juegoyninez.org',
												'from_name' => 'Corporación Juego y Niñez',
												'template_id' => $templateId], 
								'tracking' => ['opens' => true, 'html_clicks' => true, 'text_clicks' => true, 'goal_tracking' => true]]);

		// send a HTTP POST request with curl
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		$result = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$responseMailchimp = json_decode($result, true);

		// store the status message based on response code
		if ($httpCode == 200) {
			// Se actualiza la tabla local con id de camapaña mailchimp
			$campanaUpd = Notificacion::find($idListaNotificacion);
			$campanaUpd->id_campana_mailchimp = $responseMailchimp['id'];
			$campanaUpd->estado_campana = 'activa';

			if($campanaUpd->save())
			{
				$resultTransaction = true;
				$mensaje = "Creación de campaña exitosa";
			} else {
				$mensaje = "La campaña no se ha podido crear en la base de datos local para la etapa de " . strtoupper($etapa);
			}
		} else {
			$mensaje = "La campaña no se ha podido crear en mailchimp para la etapa de " . strtoupper($etapa);
		}

		return ["resultTransaction" => $resultTransaction, "mensaje" => $mensaje];
	}*/
}
