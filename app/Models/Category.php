<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $fillable = [
		'name',
		'active'
	];

	protected $visible = [
		'id',
		'name',
		'active',
		'created_at',
		'updated_at'
	];

	// --------- Relationship Methods ---------
	public function questions() {
		return $this->hasMany('App\Models\Question', 'category_id');
	}
}
