<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EvaluationType extends Model {
    protected $fillable = [
		'name',
		'active'
	];

	// --------- Setter Methods ---------
	public function setCode(string $code) {
		$code = trim($code);

		if(strlen($code) > 10) 
			throw new Exception('El código no puede tener más de 10 caracteres');

		if(strpos($code, ' ') !== false)
			throw new Exception('El código no puede tener espacios en blanco');

		$type = EvaluationType::findByCode($code);

		if($type !== null)
			throw new Exception("Ya existe un tipo de evaluación con el código [$code]");

		$this->code = $code;
	}

	// --------- Relationships Methods ---------
	public function evaluations() {
		return $this->hasMany('App\Models\Evaluation', 'evaluation_id');
	}

	// --------- Static Methods ---------
	public static function findByCode(string $code) {
		return EvaluationType::where('code', $code)->first();
	}

	public static function findByCodeOrFail(string $code) {
		$type = EvaluationType::where('code', $code)->first();

		if($type === null) 
			throw new ModelNotFoundException("No existe un tipo de evaluación con el código [$code]");

		return $type;
	}
}
