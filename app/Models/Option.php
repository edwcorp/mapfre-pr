<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Option extends Model {
    protected $fillable = [
		'label',
		'value',
		'active',
		'question_id'
	];

	// --------- Setter Methods ---------
	public function setOpenability(bool $open) {
		if(!$open)
			$this->is_open = false;
		else {
			$question = $this->question;
	
			if($question === null) 
				throw new Exception("La opción debe pertenecer a una pregunta para establecer si puede ser abierta o no");
	
			if($question->canHaveOpenOptions())
				$this->is_open = true;
			else 
				throw new Exception("La pregunta no admite opciones abiertas");
		}
	}

	// --------- Getter Methods ---------


	// --------- Relations Methods ---------
	public function question() {
		return $this->belongsTo('App\Models\Question', 'question_id');
	}
}
