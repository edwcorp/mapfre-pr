<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Answer extends Model {
    protected $fillable = [
		'evaluation_id',
		'question_id',
		'option_id',
		'filled_by',
		'about'
	];

	// --------- Setter Methods ---------
	public function setAnswer(Option $option, $answer = null) {
		$this->option_id = $option->id;
		$this->value = $option->value;
		if($option->is_open)
			$this->answer = $answer;
		else
			$this->answer = $option->label;
	}

	public function forQuestion(Question $question) {
		$this->evaluation_id = $question->evaluation_id;
		$this->question_id = $question->id;
	}

	public function answeredBy(User $user) {
		$this->filled_by = $user->id;
	}

	public function forUser(User $user) {
		$this->about = $user->id;
	}

	// --------- Getter Methods ---------

	// --------- Relationships Methods ---------
	public function filledBy() {
		return $this->belongsTo('App\User', 'filled_by');
	}

	public function about() {
		return $this->belongsTo('App\User', 'about');
	}

	public function evaluation() {
		return $this->belongsTo('App\Models\Evaluation', 'evaluation_id');
	}

	public function question() {
		return $this->belongsTo('App\Models\Question', 'question_id');
	}

	public function option() {
		return $this->belongsTo('App\Models\Option', 'option_id');
	}
}
