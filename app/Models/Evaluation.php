<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use DateTime;
use Exception;

class Evaluation extends Model {
	public static $_OUTPUT_FORMAT = 'd/m/Y H:i';
	public static $_INNER_FORMAT = 'Y-m-d H:i:s';
	public static $_PRIVACY_TYPES = [
		'public',
		'private'
	];

	protected $startDate;
	protected $endDate;

    protected $fillable = [
		'name',
		'description',
		'evaluation_type_id',
		'active'
	];

	protected $visible = [
		'id',
		'name',
		'description',
		'privacy',
		'start_date',
		'end_date',
		'evaluation_type_id',
		'active',
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'start_date' => 'date:d/m/Y - H:i',
		'end_date' => 'date:d/m/Y - H:i'
	];

	protected $appends = [
		'image_url'
	];

	// --------- Setter Methods ---------
	public function setStartDate(string $startDate) {
		$auxDate = DateTime::createFromFormat(Evaluation::$_OUTPUT_FORMAT, $startDate);

		$errors = DateTime::getLastErrors();

		if($errors['warning_count'] > 0 || $errors['error_count'] > 0)
			throw new Exception('La fecha de inicio es inválida o no está en el formato correcto');

		if(isset($this->endDate) && $auxDate >= $this->endDate)
			throw new Exception('La fecha de inicio no puede ser posterior a la fecha de finalización');

		$this->start_date = $auxDate->format(Evaluation::$_INNER_FORMAT);
		$this->startDate = $auxDate;
	}

	public function setEndDate(string $endDate) {
		$auxDate = DateTime::createFromFormat(Evaluation::$_OUTPUT_FORMAT, $endDate);

		$errors = DateTime::getLastErrors();

		if($errors['warning_count'] > 0 || $errors['error_count'] > 0)
			throw new Exception('La fecha de finalización es inválida o no está en el formato correcto');

		if(isset($this->startDate) && $auxDate <= $this->startDate)
			throw new Exception('La fecha de finalización no puede ser previa a la fecha de inicio');

		$this->end_date = $auxDate->format(Evaluation::$_INNER_FORMAT);
		$this->endDate = $auxDate;
	}

	public function setImage(Request $request, $fileName = 'image') {
		if(!isset($this->id))
			return false;

		if($request->hasFile($fileName)) {
			$finalName = $this->id . '-' . Str::random(20);
			$dir = "public/evaluations/$finalName";

			if ($this->image)
				Storage::delete($this->image);

			$this->image = $request->file($fileName)->store($dir);
			$this->save();
		}
	}

	public function setPrivacy(string $privacy) {
		$privacy = trim(strtolower($privacy));
		
		if(!in_array($privacy, Evaluation::$_PRIVACY_TYPES)) {
			$privacies = implode(', ', Evaluation::$_PRIVACY_TYPES);
			throw new Exception("la privacidad de la evaluación debe ser una de las siguientes: [$privacies]");
		}

		$this->privacy = $privacy;
	}

	// --------- Getter Methods ---------
	public function getFormattedStartDate() {
		if(!isset($this->start_date))
			return null;

		if(!isset($this->startDate))
			$this->startDate = DateTime::createFromFormat(Evaluation::$_INNER_FORMAT, $this->start_date);

		return $this->startDate->format(Evaluation::$_OUTPUT_FORMAT);
	}

	public function getFormattedEndDate() {
		if(!isset($this->end_date))
			return null;

		if(!isset($this->endDate))
			$this->endDate = DateTime::createFromFormat(Evaluation::$_INNER_FORMAT, $this->end_date);

		return $this->endDate->format(Evaluation::$_OUTPUT_FORMAT);
	}

	public function getHandableStartDate() {
		if(!isset($this->start_date))
			return null;

		if(!isset($this->startDate))
			$this->startDate = DateTime::createFromFormat(Evaluation::$_INNER_FORMAT, $this->start_date);

		return $this->startDate;
	}

	public function getHandableEndDate() {
		if(!isset($this->end_date))
			return null;

		if(!isset($this->endDate))
			$this->endDate = DateTime::createFromFormat(Evaluation::$_INNER_FORMAT, $this->end_date);

		return $this->endDate;
	}

	public function getImageUrl() {
		if(!isset($this->image))
			return null;
		return '/storage/'. $this->image;
	}

	public function isEnabled() {
		if(!$this->active)
			return false;

		$currentTime = new DateTime();
		$startTime = $this->getHandableStartDate();
		$endTime = $this->getHandableEndDate();

		// Dates are not setted
		if($startTime === null && $endTime === null)
			return false;

		// Has no start, but has an end
		if($startTime === null) 
			return $currentTime <= $endTime;

		// Has no end, but has a start
		if($endTime === null)
			return $startTime <= $currentTime;

		// Has an start and an end
		return $startTime <= $currentTime && $currentTime <= $endTime;
	}

	public function canAccess($user) {
		if(!$this->active)
			return false;

		if($this->privacy === 'public')
			return true;

		$result = $this->users()
					->where('user_id', $user->id)
					->where('active', true)->get();

		return count($result) > 0;
	}

	// --------- Relationships Methods ---------
	public function type() {
		return $this->belongsTo('App\Models\EvaluationType', 'evaluation_type_id');
	}

	public function questions() {
		return $this->hasMany('App\Models\Question', 'evaluation_id');
	}

	public function activeQuestions() {
		return $this->questions()->where('active', true)->get();
	}
	
	public function users() {
		return $this->belongsToMany('App\User', 'evaluation_user', 'evaluation_id', 'user_id');
	}
}
