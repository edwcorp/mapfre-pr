<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Exception;

class Question extends Model {
    protected $fillable = [
		'title',
		'description',
		'evaluation_id',
		'category_id',
		'active'
	];

	protected static $_TYPES = [
		'open',
		'multiple',
		'simple',
		'multiple-open',
		'simple-open'
	];

	// --------- Setter Methods ---------
	public function setType(string $type) {
		$type = trim(strtolower($type));

		if(!in_array($type, Question::$_TYPES)) {
			$types = implode(', ', Question::$_TYPES);
			throw new Exception("El tipo de pregunta debe ser una de las siguientes: [$types]");
		}

		$this->type = $type;
	}

	// --------- Getter Methods ---------
	public function canHaveOpenOptions() {
		if(!isset($this->type))
			throw new Exception('La pregunta no cuenta con un tipo setteado');

		return strpos($this->type, 'open') !== false;
	}

	public function isSimple() {
		if(!isset($this->type))
			return false;

		return strpos($this->type, 'simple') !== false;
	}

	public function isMultiple() {
		if(!isset($this->type))
			return false;

		return strpos($this->type, 'multiple') !== false;
	}

	// --------- Relationships Methods ---------
	public function evaluation() {
		return $this->belongsTo('App\Models\Evaluation', 'evaluation_id');
	}

	public function category() {
		return $this->belongsTo('App\Models\Category', 'category_id');
	}

	public function options() {
		return $this->hasMany('App\Models\Option', 'question_id');
	}

	public function activeOptions() {
		return $this->options()->where('active', true)->get();
	}
}
