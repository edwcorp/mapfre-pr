<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    //nombre de la tabla del modelo
	protected $table = 'evaluaciones';
	//llave primaria de la tabla
	protected $primaryKey = 'idevaluacion';
	//datos que pueden ser editados en las consultas
    protected $fillable = ['nombre','estado','fechainicio','fechacierre','etapa_registro_inicio','etapa_registro_fin','etapa_programacion_inicio','etapa_programacion_fin','etapa_evidencias_inicio','etapa_evidencias_fin','etapa_calificacion_inicio','etapa_calificacion_fin'];

	/*
	Relaciones
	*/ 
	/*public function convocatorias(){
        return $this->belongsTo('App\Models\Convocatoria','idconcurso','idconcurso');
	}
	
	public function inscritos(){
        return $this->hasMany('App\Models\Inscrito','idconcurso','idconcurso');
	}*/
}
