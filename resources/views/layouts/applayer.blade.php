<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		@include('includes.headauth') 
	</head>
	<body class="">
		<!--- <div id="global-loader"></div> -->
		<div class="page">
			<div class="page-main">
				<div class="header py-4">
					<div class="container">
						<div class="d-flex">
							<a class="header-brand" href="/listado/trivias">
								<img src="/assets/images/brand/logo.png" class="header-brand-img" alt="Mapfre logo">
							</a>
						</div>
					</div>
				</div>
				@yield('content')
			</div>

			<!--footer-->
			<footer class="footer">
				<div class="container">
					<div class="row align-items-center flex-row-reverse">
						<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
							Copyright © 2020 <a href="#">Mapfre</a>. Diseñado por <a href="http://digiwaycorp.com/index.php">Digiway S.A.S.</a> All rights reserved.
						</div>
					</div>
				</div>
			</footer>
			<!-- End Footer-->
		</div>
		
		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fa fa-angle-up"></i></a>
		
		@include('includes.scriptsauth')		
	</body>
</html>
