<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
        @include('includes.head') 
  </head>
	<body class="login-img">
		<div id="global-loader" ></div>
		<div class="page">
			<div class="page-single">
				<div class="container">
					<div class="row">
						<div class="col col-login mx-auto">
							<div class="text-center mb-6">
								<img src="{{ URL::asset('assets/images/brand/logo.png') }}" class="h-6" alt="">
                            </div>
                            @yield('content')
						</div>
					</div>
				</div>
			</div>
        </div>
        
        @include('includes.scripts')
	</body>
</html>