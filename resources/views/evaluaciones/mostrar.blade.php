@extends('layouts.default')
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title">Consultar Concurso {{$concurso->nombre}}</h4>
            </div>
            <div class="card-content">
               {!!Form::model($concurso,array('action'=>array('ConcursosController@actualizar',$concurso->idconcurso),'method'=>'post','class'=>'form-horizontal'))!!}
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('nombre','Nombre',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->nombre}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('estado','Estado',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->estado}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('fechainicio','Fecha de Inicio',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->fechainicio}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('fechacierre','Fecha de Cierre',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->fechacierre}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_registro_inicio','Etapa Registro - Inicio',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_registro_inicio}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_registro_fin','Etapa Registro - Fin',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_registro_fin}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_programacion_inicio','Etapa Programación - Inicio',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_programacion_inicio}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_programacion_fin','Etapa Programación - Fin',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_programacion_fin}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_evidencias_inicio','Etapa Evidencias - Inicio',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_evidencias_inicio}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_evidencias_fin','Etapa Evidencias - Fin',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_evidencias_fin}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_calificacion_inicio','Etapa Calificación - Inicio',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_calificacion_inicio}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_calificacion_fin','Etapa Calificación - Fin',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {{$concurso->etapa_calificacion_fin}}
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="hr-line-dashed"></div>
                  <button type="button" class="btn btn-outline btn-primary" 
                     onclick="document.location.href='{{ URL::to('concursos/'.$concurso->idconvocatoria.'/listado') }}'">Volver</button></center>
               </fieldset>
               {!!Form::close()!!}
            </div>
         </div>
         <!-- end card -->
      </div>
      <!-- end col-md-12 -->
   </div>
   <!-- end row -->
</div>
@stop