@extends('layouts.default')
@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <h4 class="card-title">Editar Concurso {{$concurso->nombre}}</h4>
            </div>
            <div class="card-content">
            {!!Form::model($concurso,array('action'=>array('ConcursosController@actualizar',$concurso->idconcurso),'method'=>'post','class'=>'form-horizontal', 'files'=>true))!!}
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('nombre','Nombre',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('nombre',null,array('placeholder'=>'Ingrese el nombre de la convocatoria','class'=>'form-control'))!!}
                        <span class="help-block has-error"> {{$errors->first('nombre')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('fechainicio','Fecha de inicio del concurso',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('fechainicio',date('m-d-y h:i A', strtotime($concurso->fechainicio)),array('placeholder'=>'Seleccione la fecha de inicio de la convocatoria','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('fechainicio')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('fechacierre','Fecha de cierre del concurso',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('fechacierre',date('m-d-y h:i A', strtotime($concurso->fechacierre)),array('placeholder'=>'Seleccione la fecha de cierre de la convocatoria','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('fechacierre')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_registro_inicio','Fecha de inicio - Etapa Registro',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_registro_inicio',date('m-d-y h:i A', strtotime($concurso->etapa_registro_inicio)),array('placeholder'=>'Seleccione la fecha de inicio de la Etapa Registro','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_registro_inicio')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_registro_fin','Fecha de cierre - Etapa Registro',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_registro_fin',date('m-d-y h:i A', strtotime($concurso->etapa_registro_fin)),array('placeholder'=>'Seleccione la fecha de cierre de la Etapa Registro','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_registro_fin')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_programacion_inicio','Fecha de inicio - Etapa Programación',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_programacion_inicio',date('m-d-y h:i A', strtotime($concurso->etapa_programacion_inicio)),array('placeholder'=>'Seleccione la fecha de la Etapa Programación','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_programacion_inicio')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_programacion_fin','Fecha de cierre - Etapa Programación',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_programacion_fin',date('m-d-y h:i A', strtotime($concurso->etapa_programacion_fin)),array('placeholder'=>'Seleccione la fecha de cierre de la Etapa Programación','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_programacion_fin')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_evidencias_inicio','Fecha de inicio - Etapa Evidencias',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_evidencias_inicio',date('m-d-y h:i A', strtotime($concurso->etapa_evidencias_inicio)),array('placeholder'=>'Seleccione la fecha de inicio de la Etapa Evidencias','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_evidencias_inicio')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_evidencias_fin','Fecha de cierre - Etapa Evidencias',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_evidencias_fin',date('m-d-y h:i A', strtotime($concurso->etapa_evidencias_fin)),array('placeholder'=>'Seleccione la fecha de cierre de la Etapa Evidencias','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_evidencias_fin')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_calificacion_inicio','Fecha de inicio - Etapa Calificación',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_calificacion_inicio',date('m-d-y h:i A', strtotime($concurso->etapa_calificacion_inicio)),array('placeholder'=>'Seleccione la fecha de inicio de la Etapa Calificación','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_calificacion_inicio')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('etapa_calificacion_fin','Fecha de cierre - Etapa Calificación',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::text('etapa_calificacion_fin',date('m-d-y h:i A', strtotime($concurso->etapa_calificacion_fin)),array('placeholder'=>'Seleccione la fecha de cierre de la Etapa Calificación','class'=>'form-control datetimepicker'))!!}
                        <span class="help-block has-error"> {{$errors->first('etapa_calificacion_fin')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="form-group">
                     {!!Form::label('estado','Estado',array('class' => 'col-sm-2 control-label'))!!}
                     <div class="col-sm-10">
                        {!!Form::select('estado', [
                        'activo' => 'Activo',
                        'inactivo' => 'Inactivo'],
                        Input::old('estado'),
                        array('class'=>'form-control')
                        )!!}
                        <span class="help-block has-error"> {{$errors->first('estado')}}</span>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <div class="hr-line-dashed"></div>
                  <center>{!!Form::submit('Editar Concurso',array('class'=>'btn btn-outline btn-primary'))!!}
                     <button type="button" class="btn btn-outline btn-primary" 
                        onclick="document.location.href='{{ URL::to('concursos/'. $concurso->idconvocatoria . '/listado') }}'">Volver</button>
                  </center>
               </fieldset>
               {!!Form::close()!!}
            </div>
         </div>
         <!-- end card -->
      </div>
      <!-- end col-md-12 -->
   </div>
   <!-- end row -->
</div>
@stop