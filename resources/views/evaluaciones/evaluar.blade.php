@extends('layouts.applayer')
@section('content')
<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Evaluación de desempeño 2020</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Mis Evaluaciones</a></li>
                <li class="breadcrumb-item active" aria-current="page">Evaluar</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-body">
                        <div class=" " id="profile-log-switch">
                           <div class="fade show active ">
                              <div class="table-responsive border ">
                                    <table class="table row table-borderless w-100 m-0 ">
                                       <tbody class="col-lg-6 p-0">
                                          <tr>
                                                <td><strong>Nombres y apellidos :</strong> Juan Carlos Morales Castañeda</td>
                                          </tr>
                                          <tr>
                                                <td><strong>Documento :</strong> 800909876</td>
                                          </tr>
                                          <tr>
                                                <td><strong>Área :</strong> Tecnología</td>
                                          </tr>
                                       </tbody>
                                       <tbody class="col-lg-6 p-0">
                                          <tr>
                                                <td><strong>Regional :</strong> Barranquilla</td>
                                          </tr>
                                          <tr>
                                                <td><strong>Negocio :</strong> 9080</td>
                                          </tr>
                                          <tr>
                                                <td><strong>Tipo Evaluación :</strong> Gerencial</td>
                                          </tr>
                                       </tbody>
                                    </table>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
						<h3 class="card-title">Por favor diligencie el siguiente cuestionario teniendo en cuenta cada una de las áreas a evaluar</h3>
					</div>
                    <div class="card-body">
                        <div class="col-md-12 col-xl-12">
                            <div class="card">
                                <div class="card-header bg-primary br-tr-12 br-tl-12">
                                    <h3 class="card-title text-white">Servicio al cliente</h3>
                                    <div class="card-options ">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up text-white"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default active">
                                            <div class="panel-heading " role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    1. ¿Atiende eficientemente a los proveedores en la evaluación de materiales o equipo para la perforación?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    2. ¿Satisface los requerimientos de la gerencia de producción oportunamente?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    3. ¿Establece relaciones de cordialidad con la gerencia QHSE?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingFourth">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourt" aria-expanded="false" aria-controls="collapseFourt">
                                                    4. ¿Satisface las relaciones completamente para coordinar las contrataciones de servicios?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFourt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- panel-group -->
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-primary br-tr-12 br-tl-12">
                                    <h3 class="card-title text-white">Trabajo en equipo</h3>
                                    <div class="card-options ">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up text-white"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default active">
                                            <div class="panel-heading " role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    1. ¿Atiende eficientemente a los proveedores en la evaluación de materiales o equipo para la perforación?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    2. ¿Satisface los requerimientos de la gerencia de producción oportunamente?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    3. ¿Establece relaciones de cordialidad con la gerencia QHSE?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingFourth">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourt" aria-expanded="false" aria-controls="collapseFourt">
                                                    4. ¿Satisface las relaciones completamente para coordinar las contrataciones de servicios?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFourt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- panel-group -->
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header bg-primary br-tr-12 br-tl-12">
                                    <h3 class="card-title text-white">Liderazgo</h3>
                                    <div class="card-options ">
                                        <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up text-white"></i></a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default active">
                                            <div class="panel-heading " role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                    1. ¿Atiende eficientemente a los proveedores en la evaluación de materiales o equipo para la perforación?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    2. ¿Satisface los requerimientos de la gerencia de producción oportunamente?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    3. ¿Establece relaciones de cordialidad con la gerencia QHSE?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default mt-2">
                                            <div class="panel-heading" role="tab" id="headingFourth">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourt" aria-expanded="false" aria-controls="collapseFourt">
                                                    4. ¿Satisface las relaciones completamente para coordinar las contrataciones de servicios?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseFourt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. 
                                                        </div>   
                                                        <div class="col-lg-12 col-sm-12">
                                                        <br>
                                                        </div>  
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="form-group ">
                                                                <label class="form-label">Seleccione su calificación</label>
                                                                <div class="selectgroup selectgroup-pills">
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="HTML" class="selectgroup-input" checked="">
                                                                    <span class="selectgroup-button">  Exclente  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="CSS" class="selectgroup-input">
                                                                    <span class="selectgroup-button">  Satisfactorio  </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="PHP" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Bueno   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="JavaScript" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Regular   </span>
                                                                    </label>
                                                                    <label class="selectgroup-item">
                                                                    <input type="radio" name="value" value="Angular" class="selectgroup-input">
                                                                    <span class="selectgroup-button">   Deficiente   </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- panel-group -->
                                </div>
                            </div>
                            <div class="example">
                                <div class="btn-list text-center">
                                    <a href="#" class="btn btn-primary">Guardado parcial</a>
                                    <a href="/public/evaluaciones/1/1/1/resumen" class="btn btn-secondary">Finalizar</a>
                                    <a href="#" class="btn btn-danger">Cancelar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop