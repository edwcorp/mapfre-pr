@extends('layouts.applayer')
@section('content')
<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Evaluación de desempeño 2020 </h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Mis Evaluaciones</a></li>
                <li class="breadcrumb-item active" aria-current="page">Colaboradores</li>
            </ol>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="fluid-container">
                            <div class="row ticket-card  pb-2 border-bottom pb-3 mb-3">
                                <div class="col-md-1"><img alt="profile image" class="img-sm rounded-circle mb-4 mb-md-0" src="{{ URL::asset('assets/images/faces/female/26.jpg') }}"></div>
                                <div class="ticket-details col-md-9">
                                    <p class="text-gray mb-2">El objetivo principal de la evaluación de desempeño es medir el rendimiento y el comportamiento del trabajador en su puesto de trabajo y de manera general en la organización y sobre esa base establecer el nivel de su contribución a los objetivos de la empresa.</p>
                                    <div class="row text-gray d-md-flex d-none">
                                        <div class="col-6 d-flex">
                                            <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de inicio:</p>
                                            <p class="text-dark mr-2 mb-0 text-muted font-weight-light">25 de Febrero de 2020</p>
                                        </div>
                                        <div class="col-6 d-flex">
                                            <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de cierre:</p>
                                            <p class="mr-2 mb-0 text-muted font-weight-light">05 de Mayo de 2020</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-cards">
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="card ">
                    <div class="card-body sales-relative">
                        <h5 class="text-muted">Total evaluaciones asignadas</h5>
                        <h1 class="text-success">15</h1>
                        <i class="fa fa-bar-chart fa-2x icon-absolute bg-success text-white" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="card ">
                    <div class="card-body sales-relative">
                        <h5 class="text-muted">Total evaluaciones por iniciar</h5>
                        <h1 class="text-primary">05</h1>
                        <i class="fa fa-list-ul fa-2x icon-absolute bg-primary text-white" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="card ">
                    <div class="card-body sales-relative">
                        <h5 class="text-muted">Total evaluaciones en proceso </h5>
                        <h1 class="text-danger">04</h1>
                        <i class="fa fa-hourglass-half fa-2x icon-absolute bg-danger text-white" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="card ">
                    <div class="card-body sales-relative">
                        <h5 class="text-muted">Total evaluaciones finalizadas</h5>
                        <h1 class="text-info">06</h1>
                        <i class="fa fa-check fa-2x icon-absolute bg-info text-white" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Listado de Colaboradores</div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="datatableListEmployers" class="table table-striped table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="example_info">
                                            <thead>
                                                <tr role="row">
                                                    <th class="wd-15p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="First name: activate to sort column ascending" style="width: 106px;">Nombres</th>
                                                    <th class="wd-15p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Last name: activate to sort column ascending" style="width: 103px;">Tipo documento</th>
                                                    <th class="wd-20p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 233px;"># documento</th>
                                                    <th class="wd-15p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 109px;">Negocio</th>
                                                    <th class="wd-10p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 81px;">Regional</th>
                                                    <th class="wd-10p sorting" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Estado: activate to sort column ascending" style="width: 239px;">Estado</th>
                                                    <th class="wd-10p" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Evaluar: activate to sort column ascending" style="width: 239px;">Evaluar</th>
                                                    <th class="wd-10p" tabindex="0" aria-controls="datatableListEmployers" rowspan="1" colspan="1" aria-label="Resumen: activate to sort column ascending" style="width: 239px;">Resumen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td class="">Zorita</td>
                                                    <td>Software Engineer</td>
                                                    <td>2017/06/01</td>
                                                    <td>$115,000</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td> 
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Zenaida</td>
                                                    <td>Software Engineer</td>
                                                    <td>2018/01/04</td>
                                                    <td>$125,250</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td class="">Vanessa</td>
                                                    <td>Software Designer</td>
                                                    <td>2014/6/23</td>
                                                    <td>$765,654</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-danger">Por iniciar</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Vivian</td>
                                                    <td>Financial Controller</td>
                                                    <td>2010/02/14</td>
                                                    <td>$452,500</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td class="">Una</td>
                                                    <td>Personnel Manager</td>
                                                    <td>2014/5/22</td>
                                                    <td>$765,290</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Unity</td>
                                                    <td>Marketing Designer</td>
                                                    <td>2014/7/28</td>
                                                    <td>$34,983</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-danger">Por iniciar</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td class="">Thor</td>
                                                    <td>Developer</td>
                                                    <td>2012/08/11</td>
                                                    <td>$98,540</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-danger">Por iniciar</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Trevor</td>
                                                    <td>Systems Administrator</td>
                                                    <td>2011/05/23</td>
                                                    <td>$237,500</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td class="">Timothy</td>
                                                    <td>Office Manager</td>
                                                    <td>20016/12/11</td>
                                                    <td>$136,200</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-success">Finalizado</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-danger">Por iniciar</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$139,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                                <tr role="row" class="even">
                                                    <td class="">Sakura</td>
                                                    <td>Support Engineer</td>
                                                    <td>2010/08/19</td>
                                                    <td>$150,575</td>
                                                    <td>z.serrano@datatables.net</td>
                                                    <td><span class="badge badge-warning">En proceso</span></td>
                                                    <td><a href="/public/evaluaciones/1/1/1/evaluar" class="btn btn-yellow">Evaluar</a></td>
                                                    <td><a href="#" class="btn btn-azure">Resumen</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- table-wrapper -->
                </div>
                <!-- section-wrapper -->
            </div>
        </div>
    </div>
</div>
@stop