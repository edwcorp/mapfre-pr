<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
<meta name="msapplication-TileColor" content="#0061da">
<meta name="theme-color" content="#1643a3">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

<!-- Title -->
<title>{{ config('app.name', 'Laravel') }}</title>
<link rel="stylesheet" href="{{ URL::asset('assets/fonts/fonts/font-awesome.min.css') }}">
		
<!-- Font Family-->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">

<!-- Dashboard Css -->
		
<link href="{{ URL::asset('assets/css/dashboard.css') }}" rel="stylesheet" />

<!-- c3.js Charts Plugin -->
<link href="{{ URL::asset('assets/plugins/charts-c3/c3-chart.css') }}" rel="stylesheet" />
		
<!-- Morris.js Charts Plugin -->
<link href="{{ URL::asset('./assets/plugins/morris/morris.css') }}" rel="stylesheet" />
		
<!-- Custom scroll bar css-->
<link href="{{ URL::asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" />
		
<!---Font icons-->
<link href="{{ URL::asset('assets/plugins/iconfonts/plugin.css') }}" rel="stylesheet" />

<!-- Data table css -->
<link href="{{ URL::asset('assets/plugins/datatable/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />