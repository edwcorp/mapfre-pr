<!-- Dashboard Core -->
<script src="{{ URL::asset('./assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ URL::asset('./assets/js/vendors/bootstrap.bundle.min.js') }}"></script>
<script src="{{ URL::asset('./assets/js/vendors/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('./assets/js/vendors/selectize.min.js') }}"></script>
<script src="{{ URL::asset('./assets/js/vendors/jquery.tablesorter.min.js') }}"></script>
<script src="{{ URL::asset('./assets/js/vendors/circle-progress.min.js') }}"></script>
<script src="{{ URL::asset('./assets/plugins/rating/jquery.rating-stars.js') }}"></script>
		
<!-- Charts Plugin -->
<script src="{{ URL::asset('./assets/plugins/chart/Chart.bundle.js') }}"></script>
<script src="{{ URL::asset('./assets/plugins/chart/utils.js') }}"></script>
		
<!-- Input Mask Plugin -->
<script src="{{ URL::asset('assets/plugins/input-mask/jquery.mask.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/index1.js') }}"></script>
		
<!-- Custom scroll bar Js-->
<script src="{{ URL::asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<!-- Data tables -->
<script src="{{ URL::asset('./assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('./assets/plugins/datatable/dataTables.bootstrap4.min.js') }}"></script>

<script src="{{ URL::asset('assets/js/custom.js') }}"></script>

<script>
	$(function(e) {
		$('#datatableListEmployers').DataTable();
	} );
</script>
		
<!-- Custom Js-->