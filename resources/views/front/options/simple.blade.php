@foreach($options as $index => $option) 
	<div class="option">
		<label class="custom-control custom-radio">
			<input type="radio" class="custom-control-input" 
					name="question_{{ $option->question_id }}" value="{{ $option->id }}">
			<span class="custom-control-label">{{ $option->label }}</span>
		</label>
		@if($option->is_open)
			<input type="text" name="question_{{ $option->question_id }}_answer_{{ $option->id }}" id="" class="form-control">
		@endif
	</div>
@endforeach