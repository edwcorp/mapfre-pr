@extends('layouts.applayer')
@section('content')
<div class="container">
	<div class="row">
		<div class="card-body p-8 align-items-center text-center">
			<h2 class="text-primary display-5 font-weight-bold">Ganadores</h2>
			<hr>
			<div class="card p-4">
				<div class="table-responsive">
					<table class="table card-table table-vcenter text-left text-nowrap">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th>Puntaje</th>
								<th>Trivias</th>
								<th>Nombres</th>
								<th>Documento</th>
								<th>Telefono</th>
								<th>Email</th>
							</tr>
						</thead>
						<tbody>
							@php
								$count = 0;	
							@endphp
							@foreach ($winners as $item)
								@php
									$count++;
									$user = \App\User::find($item->filled_by);
								@endphp
								<tr>
									<th class="text-center" scope="row">{{ $count }}</th>
									<td class="text-center">{{ $item->points }}</td>
									<td class="text-center">{{ $item->trivias }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->document_id }}</td>
									<td>{{ $user->phone }}</td>
									<td>{{ $user->email }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop