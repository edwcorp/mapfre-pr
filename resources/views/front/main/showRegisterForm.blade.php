@extends('layouts.applayer')
@section('content')
@php
	$errors = session()->get('errors');
	$hasEmailError = $errors !== null && $errors->hasBag('email');
	$hasDocumentError = $errors !== null && $errors->hasBag('document');
@endphp
<div class="container">
	<div class="row">
		<div class="card-body pt-6 align-items-center text-center">
			<h2 class="text-primary display-5 font-weight-bold">BIENVENIDO AL PUERTO RICO OPEN 2020</h2>
			<p class="text-primary lead">Con Mapfre en su actividad <b>#SEGUROQUELAPASARASBIEN</b>.<br>El primer paso es <b>registrarte</b> para participar del <b>premio central</b> y algunas rifas que se harán al cierre de cada jornada:</p>
		</div>
	</div>
    <div class="row">
        <div class="col col-login mx-auto">
            <form class="card" action="{{ route('front.main.register') }}" method="post">  
				@csrf
                <div class="card-body">
					<span class="text-muted text-center w-100 d-block mb-3">¿Ya te registraste? <a href="{{ route('front.main.showSesionForm') }}" class="text-primary">Ingresa Aquí</a></span>
                    <div class="form-group">
                        <label class="form-label">Nombres y apellidos</label>
						<input type="text" class="form-control" placeholder="Ingrese sus nombres" name="nombres" required value="{{ old('nombres') }}">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Correo electrónico</label>
						<input type="email" class="form-control @if($hasEmailError) is-invalid state-invalid @endif" placeholder="Ingrese su correo" name="email" required value="{{ old('email') }}">
						@if($hasEmailError)
							@php
								$emailError = $errors->getBag('email')->first();
							@endphp
							<label class="invalid-feedback">{{ $emailError }}</label>
						@endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Móvil</label>
                        <input type="text" class="form-control" placeholder="Ingrese su móvil" name="telefono" required pattern="\d+" value="{{ old('telefono') }}">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Número de documento</label>
                        <input type="number" class="form-control @if($hasDocumentError) is-invalid state-invalid @endif" placeholder="Ingrese su número de documento" name="documento" required min="1" value="{{ old('documento') }}">
						@if($hasDocumentError)
							@php
								$documentError = $errors->getBag('document')->first();
							@endphp
							<label class="invalid-feedback">{{ $documentError }}</label>
						@endif
                    </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="terminos" required value="true"/>
                            <span class="custom-control-label">Acepto <a href="terms.html">términos y condiciones</a></span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-block">Registrarse</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@stop