@extends('layouts.applayer')
@section('content')
<div class="container">
	<div class="row">
		<div class="card-body p-8 align-items-center text-center">
			<h2 class="text-primary display-5 font-weight-bold">MUCHÍSIMAS GRACIAS</h2>
			<hr>
			<p class="text-primary lead">Te agradecemos por haber participado en el Reto <b>#SEGUROQUELAPASARASBIEN</b> con <b>Mapfre Puerto Rico</b>. <br><br>En unos minutos recibirás la información con los ganadores.<br><br>No olvides asegurar lo que más quieres, de esta manera <b>seguro que la pasarás bien</b>.</p>
			<p class="text-primary lead display-8">-- MAPFRE PR</p>
		</div>
	</div>
</div>
@stop