@extends('layouts.applayer')
@section('content')
	<div class="container">
		<div class="row">
			<div class="card-body pt-6 align-items-center text-center">
				<h2 class="text-primary display-5 font-weight-semibold">RETOS</h2>
				<p class="text-primary lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ligula ante, lacinia quis risus a, consequat aliquam massa. Etiam et mauris nunc. Integer ac commodo magna, quis euismod ligula. Quisque sodales est quis nisi pretium, auctor fermentum nunc placerat.</p>
			</div>
		</div>
		<div class="row ">
			@foreach($completedEvaluations as $evaluation) 
				@include('front.evaluations.cards.completed', ['evaluation' => $evaluation])
			@endforeach

			@if($currentEvaluation === null) 
				<div class="col-sm-12">
					<a href="{{ route('front.main.show-thank-you') }}">
						<div class="card">
							<div class="card-body text-center">
								<div class="feature">
									<h3><i class="fe fe-award"></i> Muchas gracias <i class="fe fe-award"></i></h3>
								</div>
							</div>
							<div class="card-body text-center">
								<div class="btn-list text-center">
									<button type="button" class="btn btn-success">
										<i class="fe fe-check mr-2"></i> Finalizar
									</button>
								</div>
							</div>
						</div>
					</a>
				</div>
			@else
				@include('front.evaluations.cards.pending', ['evaluation' => $currentEvaluation])
			@endif
		</div>
	</div>
@stop