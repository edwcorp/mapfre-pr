@extends('layouts.applayer')
@section('content')
@php
	$errors = session()->get('errors');
	$hasErrors = $errors !== null && $errors->hasBag('email');
@endphp
<div class="container">
	<div class="row">
		<div class="card-body pt-6 align-items-center text-center">
			<h2 class="text-primary display-5 font-weight-bold">INGRESA A TUS TRIVIAS</h2>
			<p class="text-primary lead">Con Mapfre en su actividad <b>#SEGUROQUELAPASARASBIEN</b>.</p>
		</div>
	</div>
    <div class="row">
        <div class="col col-login mx-auto">
            <div class="card">
				<form method="post">
					@csrf
					<div class="card-body">
						<div class="text-center mb-4 ">
							<span class="avatar avatar-xxl  brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
						</div>
						<span class="m-4 d-none d-lg-block text-center">
							<span class="text-dark"><strong>Ingresa tu correo electrónico</strong></span>
						</span>
						<div class="form-group">
							<input type="email" class="form-control @if(session()->has('errors')) is-invalid state-invalid @endif" name="email" placeholder="Correo electrónico" required value={{ old('email') }}>
							@if($hasErrors)
								@php
									$emailError = $errors->getBag('email')->first();
								@endphp
								<label class="invalid-feedback">{{ $emailError }}</label>
							@endif
						</div>
						<button type="submit" class="btn btn-primary btn-block">Vamos!</button>
						<span class="text-muted text-center w-100 d-block mt-3">¿No tienes cuenta? <a href="{{ route('front.main.showRegisterForm') }}">Regístrate</a></span>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>
@stop