<div class="col-sm-6 col-md-4 col-lg-3 features">
	<a href="{{ route('front.evaluation.show', $evaluation->id) }}">
		<div class="card">
			<div class="card-body text-center">
				<div class="feature">
					<div class="fa-stack fa-lg fa-1x border bg-secondary mb-3">
						<i class="fa fa-map-pin fa-stack-1x text-white"></i>
					</div>
					<h3>{{ $evaluation->name }}</h3>
				</div>
			</div>
			<div class="card-body text-center">
				<div class="btn-list text-center">
					<button type="button" class="btn btn-success">
						<i class="fe fe-play"></i> Participar
					</button>
				</div>
			</div>
		</div>
	</a>
</div>