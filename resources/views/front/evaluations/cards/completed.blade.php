<div class="col-sm-6 col-md-4 col-lg-3 features">
	<a href="{{ route('front.evaluations.results', $evaluation->id) }}">
		<div class="card">
			<div class="card-body text-center">
				<div class="feature">
					<div class="fa-stack fa-lg fa-1x border bg-primary mb-3">
						<i class="fa fa-fa fa-stack-1x text-white"></i>
					</div>
					<h3>{{ $evaluation->name }}</h3>
				</div>
			</div>
			<div class="card-body text-center">
				<div class="btn-list text-center">
					<button type="button" class="btn btn-success">
						<i class="fe fe-check mr-2"></i> Mis Resultados
					</button>
				</div>
			</div>
		</div>
	</a>
</div>