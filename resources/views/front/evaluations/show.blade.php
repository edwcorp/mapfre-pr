@extends('layouts.applayer')
@section('content')
	<div class="container">
		<div class="row mt-5">
			<div class="col-sm-12">
				<form action="{{ route('front.evaluation.answer', $evaluation->id) }}" method="post">
					@csrf
					<div class="card">
						<div class="card-header">
							<h1 class="font-weight-semibold mt-5 text-center w-100">{{ $evaluation->name }}</h1>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12">
									@if($evaluation->image) 
										<div class="bg-image">
											<img src="{{ $evaluation->image }}" alt="" class="fluid">
										</div>
									@endif
								</div>
								<div class="col-sm-12">
									<p class="lead">CUÉNTANOS LO QUE SABES DEL OPEN<p>
								</div>
							</div>
							<div class="col-sm-12">
								<h3>Preguntas:</h3>
							</div>
							<div class="col-sm-12">
								@include( 'front.questions.master', [ 'questions' => $evaluation->activeQuestions() ] )
							</div>
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-sm-6">
									<a href="{{ route('front.main.showListTrivias') }}" class="btn btn-link">
										Volver
									</a>
								</div>
								<div class="col-sm-6 text-right">
									<button type="submit" class="btn btn-primary">
										Guardar
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop