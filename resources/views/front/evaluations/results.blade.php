@extends('layouts.applayer')
@section('content')
	<div class="container">
		<div class="row mt-5">
			<div class="col-md-12 col-lg-8 offset-lg-2">
				<div class="card ">
					<div class="card-header text-center">
						<a href="{{ route('front.main.showListTrivias') }}" class="card-options-collapse">
							<i class="fe fe-arrow-left display-5"></i>
						</a>
						<h2 class="font-weight-semibold mt-5 text-center w-100">{{ $evaluation->name }}</h2>
					</div>
					<div class="card-body text-center pb-5">
						<div class="row">
							<div class="col-sm-12">
								<p class="lead display-5">{{ $evaluation->description }}</p>
							</div>
							<div class="col-sm-12">
								@if($correctAnswers === 0)
									<i class="fe fe-x-circle display-1 text-danger"></i>
									<h2 class="mt-5 mb-5">Usted no tuvo respuestas correctas!</h2>
								@else
									@php
										$plural = $correctAnswers > 1;
									@endphp
									<i class="fe fe-check-circle display-1 text-success"></i>
									<h2 class="mt-5 mb-5">Usted tuvo {{ $correctAnswers }} respuesta{{$plural ? 's' : ''}} correcta{{$plural ? 's' : ''}}!</h2>
								@endif
							</div>
						</div>
					</div>
					<class class="card-footer">
						<a href="{{ route('front.main.showListTrivias') }}" class="btn btn-link">Volver</a>
					</class>
				</div>
			</div>
		</div>
	</div>
@stop