@foreach($questions as $index => $question)
	<div class="question @if($question->category_id === 3) for-information @endif form-group">
		<div class="title display-3 font-weight-bold form-label">
			{{ $index + 1 }}. {{ $question->title }}
			@if($question->category_id === 3)
				<span class="text-muted font-weight-light">
					* Esta pregunta no suma puntos
				</span>
			@endif
		</div>
		@if($question->description)
			<div class="description">
				<p>{{ $question->description }}</p>
			</div>
		@endif
		<div class="custom-controls-stacked">
			@if($question->isSimple())
				{{-- Render options as simple selection for the question --}}
				@include( 'front.options.simple', [ 
					'options' => $question->activeOptions(),
					'questionId' => $question->id
				])
			@elseif ($question->isMultiple())
				{{-- Render options as multiple selection for the question --}}
			@else
				{{-- Render options as open answer for the question --}}
			@endif
		</div>
	</div>
@endforeach