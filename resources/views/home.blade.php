@extends('layouts.applayer')

@section('content')

<div class="my-3 my-md-5">
    <div class="container">
        <div class="page-header">
            <h4 class="page-title">Dashboard</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard 01</li>
            </ol>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Gestionar Evaluaciones - Activas</h3>
                </div>
                <div class="card-body">
                    <div class="fluid-container">
                        <div class="row ticket-card  pb-2 border-bottom pb-3 mb-3">
                            <div class="col-md-1"><img alt="profile image" class="img-sm rounded-circle mb-4 mb-md-0" src="{{ URL::asset('assets/images/faces/female/26.jpg') }}"></div>
                            <div class="ticket-details col-md-9">
                                <div class="d-flex">
                                    <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Evaluación de desempeño 2019</p>
                                </div>
                                <p class="text-gray mb-2">El objetivo principal de la evaluación de desempeño es medir el rendimiento y el comportamiento del trabajador en su puesto de trabajo y de manera general en la organización y sobre esa base establecer el nivel de su contribución a los objetivos de la empresa.</p>
                                <div class="row text-gray d-md-flex d-none">
                                    <div class="col-6 d-flex">
                                        <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de inicio:</p>
                                        <p class="text-dark mr-2 mb-0 text-muted font-weight-light">25 de Febrero de 2020</p>
                                    </div>
                                    <div class="col-6 d-flex">
                                        <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de cierre:</p>
                                        <p class="mr-2 mb-0 text-muted font-weight-light">05 de Mayo de 2020</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ticket-actions col-md-2">
                                <div class="btn btn-group dropdown">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-oblong btn-sm btn-primary dropdown-toggle btn btn-sm" data-toggle="dropdown" type="button">Gestionar</button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="evaluaciones/1/1/listado"><i class="fa fa-reply fa-fw"></i>Ir a la evaluación</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-bar-chart  fa-fw"></i>Ir a reportes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title ">Gestionar Evaluaciones - Inactivas</h3>
                </div>
                <div class="card-body">
                    <div class="fluid-container">
                        <div class="row ticket-card  pb-2 border-bottom pb-3 mb-3">
                            <div class="col-md-1"><img alt="profile image" class="img-sm rounded-circle mb-4 mb-md-0" src="assets/images/faces/female/26.jpg"></div>
                            <div class="ticket-details col-md-9">
                                <div class="d-flex">
                                    <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Evaluación de desempeño 2018</p>
                                </div>
                                <p class="text-gray mb-2">El objetivo principal de la evaluación de desempeño es medir el rendimiento y el comportamiento del trabajador en su puesto de trabajo y de manera general en la organización y sobre esa base establecer el nivel de su contribución a los objetivos de la empresa.</p>
                                <div class="row text-gray d-md-flex d-none">
                                    <div class="col-6 d-flex">
                                        <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de inicio:</p>
                                        <p class="text-dark mr-2 mb-0 text-muted font-weight-light">25 de Febrero de 2019</p>
                                    </div>
                                    <div class="col-6 d-flex">
                                        <p class="text-dark font-weight-bold mr-2 mb-0 text-nowrap">Fecha de cierre:</p>
                                        <p class="mr-2 mb-0 text-muted font-weight-light">05 de Mayo de 2019</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ticket-actions col-md-2">
                                <div class="btn btn-group dropdown">
                                    <button aria-expanded="false" aria-haspopup="true" class="btn btn-oblong btn-sm btn-primary dropdown-toggle btn btn-sm" data-toggle="dropdown" type="button">Gestionar</button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#"><i class="fa fa-reply fa-fw"></i>Ir a la evaluación</a>
                                        <a class="dropdown-item" href="#"><i class="fa fa-bar-chart fa-fw"></i>Ir a reportes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection