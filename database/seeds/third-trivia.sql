-- Evaluación principal de la actividad
insert into
evaluations
set
name = "Hoyo 10",
description = "CUÉNTANOS LO QUE SABES DEL OPEN",
privacy = "private", 
start_date = "2020/02/12 07:00:00",
end_date = "2020/03/12 07:00:00",
evaluation_type_id = "1";

-- 1ra pregunta
insert into
questions
set
title = "¿A qué categoría del PGA corresponde el Puerto Rico Open?",
type = "simple",
evaluation_id = 3,
category_id = 1;

insert into
options
set
label = "Major Championships",
value = 0,
is_open = 0,
question_id = 7;

insert into
options
set
label = "World Golf Championships",
value = 0,
is_open = 0,
question_id = 7;

insert into
options
set
label = "Fedexc Cup Playoff Events",
value = 0,
is_open = 0,
question_id = 7;

insert into
options
set
label = "Other Tournaments",
value = 1,
is_open = 0,
question_id = 7;

-- 2a pregunta
insert into
questions
set
title = "Pregunta MAPFRE",
type = "simple",
evaluation_id = 3,
category_id = 2;

insert into
options
set
label = "Opción 1",
value = 0,
is_open = 0,
question_id = 8;

insert into
options
set
label = "Opción 2",
value = 0,
is_open = 0,
question_id = 8;

insert into
options
set
label = "Opción 3",
value = 1,
is_open = 0,
question_id = 8;

insert into
options
set
label = "Opción 4",
value = 0,
is_open = 0,
question_id = 8;

-- 3a pregunta
insert into
questions
set
title = "¿En tu hogar hay mascotas?",
type = "simple",
evaluation_id = 3,
category_id = 3;

insert into
options
set
label = "Sí",
value = 0,
is_open = 0,
question_id = 9;

insert into
options
set
label = "No",
value = 0,
is_open = 0,
question_id = 9;