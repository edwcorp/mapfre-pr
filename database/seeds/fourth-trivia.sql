-- Evaluación principal de la actividad
insert into
evaluations
set
name = "Hoyo 18",
description = "CUÉNTANOS LO QUE SABES DEL OPEN",
privacy = "private", 
start_date = "2020/02/12 07:00:00",
end_date = "2020/03/12 07:00:00",
evaluation_type_id = "1";

-- 1ra pregunta
insert into
questions
set
title = "¿Cuáles de los siguientes son patrocinadores del PR OPEN?",
type = "simple",
evaluation_id = 4,
category_id = 1;

insert into
options
set
label = "Mapfre, T-Mobile, FedEx",
value = 0,
is_open = 0,
question_id = 10;

insert into
options
set
label = "Mapfre, Medalla, FedEx",
value = 1,
is_open = 0,
question_id = 10;

insert into
options
set
label = "Mapfre, Medalla, Hewlett Packard",
value = 0,
is_open = 0,
question_id = 10;

-- 2a pregunta
insert into
questions
set
title = "Pregunta MAPFRE",
type = "simple",
evaluation_id = 4,
category_id = 2;

insert into
options
set
label = "Opción 1",
value = 0,
is_open = 0,
question_id = 11;

insert into
options
set
label = "Opción 2",
value = 0,
is_open = 0,
question_id = 11;

insert into
options
set
label = "Opción 3",
value = 1,
is_open = 0,
question_id = 11;

insert into
options
set
label = "Opción 4",
value = 0,
is_open = 0,
question_id = 11;

-- 3a pregunta
insert into
questions
set
title = "¿Tu compañía cuenta con seguros para el grupo de empleados?",
type = "simple",
evaluation_id = 4,
category_id = 3;

insert into
options
set
label = "Sí",
value = 0,
is_open = 0,
question_id = 12;

insert into
options
set
label = "No",
value = 0,
is_open = 0,
question_id = 12;

-- 4a pregunta
insert into
questions
set
title = "¿Cuentas con alguna protección para tus dispositivos móviles y de
escritorio?",
type = "simple",
evaluation_id = 4,
category_id = 3;

insert into
options
set
label = "Sí",
value = 0,
is_open = 0,
question_id = 13;

insert into
options
set
label = "No",
value = 0,
is_open = 0,
question_id = 13;