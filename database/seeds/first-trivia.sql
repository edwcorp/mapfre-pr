-- Evaluación principal de la actividad
insert into
evaluations
set
name = "Hoyo 1",
description = "CUÉNTANOS LO QUE SABES DEL OPEN",
privacy = "public", 
start_date = "2020/02/12 07:00:00",
end_date = "2020/03/12 07:00:00",
evaluation_type_id = "1";

-- 1ra pregunta
insert into
questions
set
title = "¿Qué versión del OPEN PR es esta?",
type = "simple",
evaluation_id = 1,
category_id = 1;

insert into
options
set
label = "10ª",
value = 0,
is_open = 0,
question_id = 1;

insert into
options
set
label = "15ª",
value = 0,
is_open = 0,
question_id = 1;

insert into
options
set
label = "13ª",
value = 1,
is_open = 0,
question_id = 1;

insert into
options
set
label = "12ª",
value = 0,
is_open = 0,
question_id = 1;

-- 2a pregunta
insert into
questions
set
title = "Pregunta MAPFRE",
type = "simple",
evaluation_id = 1,
category_id = 2;

insert into
options
set
label = "Opción 1",
value = 0,
is_open = 0,
question_id = 2;

insert into
options
set
label = "Opción 2",
value = 0,
is_open = 0,
question_id = 2;

insert into
options
set
label = "Opción 3",
value = 1,
is_open = 0,
question_id = 2;

insert into
options
set
label = "Opción 4",
value = 0,
is_open = 0,
question_id = 2;

-- 3a pregunta
insert into
questions
set
title = "Actualmente Estás",
type = "simple",
evaluation_id = 1,
category_id = 3;

insert into
options
set
label = "Soltero/a",
value = 0,
is_open = 0,
question_id = 3;

insert into
options
set
label = "Soltero/a con Hijos",
value = 0,
is_open = 0,
question_id = 3;

insert into
options
set
label = "Casado/a",
value = 0,
is_open = 0,
question_id = 3;

insert into
options
set
label = "Casado/a con Hijos",
value = 0,
is_open = 0,
question_id = 3;
