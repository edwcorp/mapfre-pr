-- Evaluación principal de la actividad
insert into
evaluations
set
name = "Hoyo 9",
description = "CUÉNTANOS LO QUE SABES DEL OPEN",
privacy = "private", 
start_date = "2020/02/12 07:00:00",
end_date = "2020/03/12 07:00:00",
evaluation_type_id = "1";

-- 1ra pregunta
insert into
questions
set
title = "¿Quién fue el campeón de la versión Open 2019?",
type = "simple",
evaluation_id = 2,
category_id = 1;

insert into
options
set
label = "D.A. Points",
value = 0,
is_open = 0,
question_id = 4;

insert into
options
set
label = "Martin Trainer",
value = 1,
is_open = 0,
question_id = 4;

insert into
options
set
label = "Tony Finau",
value = 0,
is_open = 0,
question_id = 4;

insert into
options
set
label = "Ninguno de los Anteriores",
value = 0,
is_open = 0,
question_id = 4;

-- 2a pregunta
insert into
questions
set
title = "Pregunta MAPFRE",
type = "simple",
evaluation_id = 2,
category_id = 2;

insert into
options
set
label = "Opción 1",
value = 0,
is_open = 0,
question_id = 5;

insert into
options
set
label = "Opción 2",
value = 0,
is_open = 0,
question_id = 5;

insert into
options
set
label = "Opción 3",
value = 1,
is_open = 0,
question_id = 5;

insert into
options
set
label = "Opción 4",
value = 0,
is_open = 0,
question_id = 5;

-- 3a pregunta
insert into
questions
set
title = "¿Cuentas con algún seguro de protección de bienes?",
type = "simple-open",
evaluation_id = 2,
category_id = 3;

insert into
options
set
label = "Terremoto",
value = 0,
is_open = 0,
question_id = 6;

insert into
options
set
label = "Multiplan",
value = 0,
is_open = 0,
question_id = 6;

insert into
options
set
label = "Inundación",
value = 0,
is_open = 0,
question_id = 6;

insert into
options
set
label = "Asistencia al Hogar",
value = 0,
is_open = 0,
question_id = 6;

insert into
options
set
label = "No Tengo",
value = 0,
is_open = 0,
question_id = 6;

insert into
options
set
label = "Otro, ¿Cuál?",
value = 0,
is_open = 1,
question_id = 6;
