<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('answers', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('evaluation_id')->unsigned();
			$table->bigInteger('question_id')->unsigned();
			$table->bigInteger('option_id')->unsigned();
			$table->bigInteger('filled_by')->unsigned();
			$table->bigInteger('about')->unsigned()->nullable();
			$table->integer('value');
			$table->longText('answer');
			$table->timestamps();
			
			$table->foreign('evaluation_id')->references('id')->on('evaluations');
			$table->foreign('question_id')->references('id')->on('questions');
			$table->foreign('option_id')->references('id')->on('options');
			$table->foreign('filled_by')->references('id')->on('users');
			$table->foreign('about')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('answers');
    }
}
