<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('questions', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('title', 200);
			$table->longtext('description')->nullable();
			$table->enum('type', [
				'open',
				'multiple',
				'simple',
				'multiple-open',
				'simple-open'
			]);
			$table->bigInteger('evaluation_id')->unsigned();
			$table->bigInteger('category_id')->unsigned()->nullable();
			$table->boolean('active')->default(true);
			$table->timestamps();
			
			$table->foreign('evaluation_id')->references('id')->on('evaluations');
			$table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('questions');
    }
}
