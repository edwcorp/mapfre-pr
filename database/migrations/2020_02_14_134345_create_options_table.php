<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('options', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('label', 200);
			$table->integer('value');
			$table->boolean('is_open')->default(false);
			$table->bigInteger('question_id')->unsigned();
			$table->boolean('active')->default(true);
			$table->timestamps();
			
			$table->foreign('question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('options');
    }
}
