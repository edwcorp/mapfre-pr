<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('evaluations', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name', 200);
			$table->longText('description')->nullable();
			$table->enum('privacy', [
				'public',
				'private'
			]);
			$table->dateTime('start_date')->nullable();
			$table->dateTime('end_date')->nullable();
			$table->string('image')->nullable();
			$table->bigInteger('evaluation_type_id')->unsigned();
			$table->boolean('active')->default(true);
			$table->timestamps();
			
			$table->foreign('evaluation_type_id')->references('id')->on('evaluation_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('evaluations');
    }
}
