<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Front End Routes
|--------------------------------------------------------------------------
*/

Route::group(
	[
		'namespace' => '\App\Http\Controllers\Front', 
		'middleware' => 'cookie.token'
	], 
	function () {
		// --------- Evaluations Routes ---------
		Route::get('/trivia/{evaluationId}', 'EvaluationsController@show')->defaults('_config', [
			'view' => 'front.evaluations.show'
		])->name('front.evaluation.show');

		Route::post('/trivia/{evaluationId}', 'EvaluationsController@answer')->defaults('_config', [
			'redirect' => 'front.evaluations.results'
		])->name('front.evaluation.answer');

		Route::get('/resultados-trivia/{evaluationId}', 'EvaluationsController@results')->defaults('_config', [
			'view' => 'front.evaluations.results',
			'redirect' => 'front.evaluation.show'
		])->name('front.evaluations.results');

		// --------- Trivias List Routes ---------
		Route::get('/listado/trivias', 'MainController@showListTrivias')->defaults('_config', [
			'view' => 'front.main.showListTrivias'
		])->name('front.main.showListTrivias');

		// --------- Static Content Routes ---------
		Route::get('/gracias', 'MainController@showThankYou')->defaults('_config', [
			'view' => 'front.main.thank-you'
		])->name('front.main.show-thank-you');
	}
); 

// Front End Routes with no security
Route::group(
	[
		'namespace' => '\App\Http\Controllers\Front'
	], 
	function () {
		// --------- Register Routes ---------
		Route::get('/', 'MainController@showRegisterForm')->defaults('_config', [
			'view' => 'front.main.showRegisterForm'
		])->name('front.main.home');

		Route::get('/registro', 'MainController@showRegisterForm')->defaults('_config', [
			'view' => 'front.main.showRegisterForm'
		])->name('front.main.showRegisterForm');

		Route::post('/registro', 'MainController@storeRegisterForm')->defaults('_config', [
			'redirect' => 'front.main.showListTrivias'
		])->name('front.main.register');

		// --------- Login Routes ---------
		Route::get('/sesion', 'MainController@showSesionForm')->defaults('_config', [
			'view' => 'front.main.showSesionForm'
		])->name('front.main.showSesionForm');

		Route::post('/sesion', 'MainController@login')->defaults('_config', [
			'redirect' => 'front.main.showListTrivias'
		])->name('front.main.login');

		// --------- Results Routes ---------
		Route::get('/ganadores', 'MainController@winners')->defaults('_config', [
			'view' => 'front.main.winners'
		])->name('front.main.winners');
	}
); 

/*
|--------------------------------------------------------------------------
| Back End Routes
|--------------------------------------------------------------------------
*/
/* 
Route::group(
	[
		'namespace' => '\App\Http\Controllers\Admin', 
		'middleware' => 'backend'
	], 
	function () {
	}
); 

// admin End Routes with no security
Route::group(
	[
		'namespace' => '\App\Http\Controllers\admin'
	], 
	function () {
	}
);  */